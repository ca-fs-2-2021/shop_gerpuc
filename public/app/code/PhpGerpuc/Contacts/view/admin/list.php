<hr>
<div class='mx-5'>
    <div class="contact-list"></div>

    <?php
    foreach ($data['contacts'] as $contact) :
        ?>

        <table class=" row table">

            <td class="name">
                <p> Name: </p>
                <?= $contact->getName() ?>
            </td>

            <td class="surname" >
                <p> Surname: </p>
                <?= $contact->getSurname() ?>
            </td>

            <td class="email">
                <p> Email: </p>
                <?= $contact->getEmail() ?>
            </td>
            <td class="phone">
                <p> Phone: </p>
                <?= $contact->getPhone() ?>
            </td>
            <td class="subject">
                <p> Subject: </p>
                <?= $contact->getSubject() ?>
            </td>
            <td class="message">
                <p> Subject: </p>
                <?= $contact->getMessage() ?>
            </td>

            <td>
                <form method="post" action="/contacts/delete">
                    <p> Delete: </p>
                    <input type="hidden" name="id" value="<?= $contact->getId() ?>">
                    <input type="submit" value="X">
                </form>
            </td>
            </tr>
        </table>

    <?php endforeach; ?>
</div>
<hr>