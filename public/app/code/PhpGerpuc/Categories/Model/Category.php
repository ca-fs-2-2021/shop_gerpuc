<?php

namespace PhpGerpuc\Categories\Model;

use \PhpGerpuc\Framework\Helper\SqlBuilder;

class Category
{
    private $id;
    private $name;
    private $slug;
    private $parent_id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return mixed
     */
    public function getParent_id()
    {
        return $this->parent_id;
    }

    /**
     * @param mixed $parent_id
     */
    public function setParent_id($parent_id): void
    {
        $this->parent_id = $parent_id;
    }

    public function load($id)
    {
        $db = new SqlBuilder();
        $category = $db->select()->from('categories')->where('id', $id)->getOne();
        $this->id = $category['id'];
        $this->name = $category['name'];
        $this->slug = $category['slug'];
        $this->parent_id = $category['parent_id'];
        return $this;
    }

    public function save()
    {
        if (isset($this->id) ) {
            $this->update();
        } else {
            $this->create();
        }
    }

    private function update()
    {
        $db = new SqlBuilder();
        $category = [
            'name' => $this->name,
            'slug' => $this->slug,
            'parent_id' => $this->parent_id,
        ];

        $db->update('categories')->set($category)->where('id', $this->id)->exec();
    }

    private function create()
    {
        $db = new SqlBuilder();
        $db->insert('categories')->values([
            'name' => $this->name,
            'slug' => $this->slug,
            'parent_id' => $this->parent_id
        ])->exec();
    }

    public function delete($id)
    {
        $db = new SqlBuilder();
        $db->delete()->from('categories')->where('id', $id)->exec();
    }
}
