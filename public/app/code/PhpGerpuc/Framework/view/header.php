<?php

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">    <title>Document</title>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Shop</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../index.php">Home Page</a>
                </li>
                <li>
                    <a class="nav-link" href="/products/"> Products</a>
                </li>
                <li>
                    <a class="nav-link" href="/products/create">Create Products</a>
                </li>
                <li>
                    <a class="nav-link" href="/categories/create">Create Categories</a>
                </li>
                <li>
                    <a class="nav-link" href="/contacts/create">Contacts Form</a>
                </li>
                <li>
                    <a class="nav-link" href="/contacts/">Contact Messages</a>
                </li>
                <li>
                    <a class="nav-link" href="/contactscategories/create">Create Contacts categories</a>
                </li>

            </ul>
        </div>
    </nav>

    <main>