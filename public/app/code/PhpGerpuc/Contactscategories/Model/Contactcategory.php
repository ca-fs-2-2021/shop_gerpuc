<?php

namespace PhpGerpuc\ContactsCategories\Model;

use \PhpGerpuc\Framework\Helper\SqlBuilder;

class Contactcategory
{
    private $id;
    private $name;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function load($id)
    {
        $db = new SqlBuilder();
        $contactcategory = $db->select()->from('contactscategories')->where('id', $id)->getOne();
        $this->id = $contactcategory['id'];
        $this->name = $contactcategory['name'];
        return $this;
    }

    public function save()
    {
        if (isset($this->id) ) {
            $this->update();
        } else {
            $this->create();
        }
    }

    private function update()
    {
        $db = new SqlBuilder();
        $contactcategory = [
            'name' => $this->name

        ];

        $db->update('contactscategories')->set($contactcategory)->where('id', $this->id)->exec();
    }

    private function create()
    {
        $db = new SqlBuilder();
        $db->insert('contactscategories')->values([
            'name' => $this->name,
        ])->exec();
    }

    public function delete($id)
    {
        $db = new SqlBuilder();
        $db->delete()->from('contactscategories')->where('id', $id)->exec();
    }
}
