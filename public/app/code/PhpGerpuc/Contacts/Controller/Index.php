<?php

namespace PhpGerpuc\Contacts\Controller;

use PhpGerpuc\Framework\Core\Controller;
use PhpGerpuc\Framework\Helper\FormBuilder;
use PhpGerpuc\Framework\Helper\Request;
use \PhpGerpuc\Framework\Helper\Url;
use \PhpGerpuc\Contacts\Model\Contact;
use \PhpGerpuc\Contacts\Model\Collection\Contacts;
use \PhpGerPuc\Contactscategories\Model\Collection\Contactscategories;

class Index extends Controller
{
    private $post;

    public function __construct()
    {
        $request = new Request();
        $this->post = $request->getPost();
        parent::__construct('PhpGerpuc\Contacts');
    }

    public function index()
    {
        $data['title'] = 'Contacts list';
        $contactsCollection = new Contacts();
        $data['contacts'] = $contactsCollection->getCollection();
        $this->render('admin/list', $data);
    }

    public function create()
    {
        $contactscategories = new ContactsCategories();

        $form = new FormBuilder('POST', Url::getUrl('contacts/store'), 'form-group form', 'product-form');
        $form->input('text', 'name', 'form-control', 'name', ' Name', '', '','','',true)
            ->input('text', 'surname', 'form-control', 'surname', ' Surname', '', '', '', '', true)
            ->input('text', 'email', 'form-control', 'email', 'Email', '', '','','', true)
            ->input('number', 'phone', 'form-control', 'phone', 'Phone')
            ->input('text', 'subject', 'form-control', 'subject', 'Subject','', '','','',true)
            ->textarea('message', 'Message', 'message', '', true)
            ->button('ok', 'Send message', 'btn btn-info');

        foreach ($contactscategories->getCollection() as $contactcategory) {
            $form->input(
                'checkbox',
                'categories[]',
                'form-check-input mx-2',
                'category' . $contactcategory->getId(),
                '',
                $contactcategory->getName(),
                '',
                $contactcategory->getId()
            );
        }

        $data['title'] = 'Write us a message;)';
        $data['form'] = $form->get();
        $this->render('form/create', $data);

    }

    public function store()
    {
        $contact = new Contact();

        $contact->setName($this->post['name']);
        $contact->setSurname($this->post['surname']);
        $contact->setEmail($this->post['email']);
        $contact->setPhone($this->post['phone']);
        $contact->setSubject($this->post['subject']);
        $contact->setMessage($this->post['message']);
        $contact->save();
        header('Location:' . Url::getUrl('contacts'));
    }

    public function delete()
    {
        $this->id = $this->post['id'];
        $contact = new Contact();
        $contact->load($this->id)->delete();
        header('Location: http://127.0.0.1:8001/contacts');
    }
}

