<?php

namespace PhpGerpuc\Contactscategories\Model\Collection;

use PhpGerpuc\Framework\Helper\SqlBuilder;
use PhpGerpuc\Contactscategories\Model\Contactcategory;

class Contactscategories
{
    private $collection = [];

    public function getCollection()
    {
        $db = new SqlBuilder();
        $categoriesIds = $db->select('id')->from('contactscategories')->get();

        foreach ($categoriesIds as $row) {
            $contactcategory = new Contactcategory();
            $this->collection[] = $contactcategory->load($row['id']);
        }
        return $this->collection;
    }
}