<?php

namespace PhpGerpuc\Framework\Helper;

use PhpGerpuc\Errors\Controller\Index as Error;

class Routing
{
    public const ROUTING_MAP_FILE = 'routes.php';
    public const DEFAULT_CONTROLLER_METHOD = 'index';
    public const DEFAULT_HOME_CONTROLLER = '\PhpGerpuc\Home\Controller\Index';

    public function getControllerClass($url)
    {
        include_once CONFIG_DIR . '/' . self::ROUTING_MAP_FILE;
        $method = self::DEFAULT_CONTROLLER_METHOD;
        $params = null;

        // * $url = [route => products, methos=>show, param=>1]


        $keyFromRoutingFile = $url['route'];
        if ($url['route'] !== '/') {

        if (isset($routes[$keyFromRoutingFile])) { //is isset $routes['products]
            $controllerClass = $routes[$keyFromRoutingFile];
            isset($url['method']) ? $method = $url['method'] : $method = self::DEFAULT_CONTROLLER_METHOD; //is method isset
            isset($url['param']) ? $params = $url['param'] : $params = null; // is param isset 
        } else {
            $controllerClass = $routes['error']; // if not, get error
        }
    }else {
        $controllerClass = self::DEFAULT_HOME_CONTROLLER;

    }

        
        $controller = new $controllerClass;
        if (method_exists($controller, $method)) {
            if ($params !== null) {
                $controller->$method($params);
            } else {
                $controller->$method();
            }
        } else {
            $controller = new Error();
            $controller->error404();
        }
    }
}
