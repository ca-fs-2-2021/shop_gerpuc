<?php

namespace PhpGerpuc\Contacts\Model;

use \PhpGerpuc\Framework\Helper\SqlBuilder;
use \PhpGerpuc\Framework\Helper\Validation;

class Contact
{
    private $id;
    private $name;
    private $surname;
    private $email;
    private $phone;
    private $subject;
    private $message;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setSurname($surname): void
    {
        $this->surname = $surname;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message): void
    {
        $this->message = $message;
    }

    public function load($id)
    {
        $db = new SqlBuilder();
        $contact = $db->select()->from('contacts')->where('id', Validation::validInteger($id))->getOne();
        $this->id = $contact['id'] ;
        $this->name = $contact['name'] ;
        $this->surname = $contact['surname'] ;
        $this->email = $contact['email'];
        $this->phone = $contact['phone'];
        $this->subject = $contact['subject'] ;
        $this->message = $contact['message'] ;
        return $this;
    }

    public function save()
    {
            $this->create();
    }

    private function create()
    {
        $contact = [
            'name' => Validation::validEmail($this->name),
            'surname' => Validation::validString($this->surname),
            'email' => $this->email,
            'phone' => $this->phone,
            'subject' => Validation::validString($this->subject),
            'message' => $this->message
        ];
        $db = new SqlBuilder();
        $db->insert('contacts')->values($contact)->exec();
    }

    public function delete()
    {
        $db = new SqlBuilder();
        $db->delete()->from('contacts')->where('id', $this->id)->exec();
    }





}
