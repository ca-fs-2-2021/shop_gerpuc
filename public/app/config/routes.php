<?php

$routes = [
    'products' => 'PhpGerpuc\Products\Controller\Index',
    'contacts' => 'PhpGerpuc\Contacts\Controller\Index',
    'contactscategories' => 'PhpGerpuc\Contactscategories\Controller\Index',
    'categories' => 'PhpGerpuc\Categories\Controller\Index',
    'error' => 'PhpGerpuc\Error\Controller\Index',
    '\\' => 'PhpGerpuc\Home\Controller\Index'

];