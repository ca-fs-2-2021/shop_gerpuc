<hr>
<div class='mx-5'>
<div class="products-list"></div>

<?php
foreach ($data['products'] as $product) :
?>

    <table class=" row table">

        <td class="product-name">
            <p> Name: </p>
            <?= $product->getName() ?>
        </td>

        <td class="product-sku">
            <p> SKU: </p>
            <?= $product->getSku() ?>
        </td>

        <td class="product-qty">
            <p> QTY: </p>
            <?= $product->getQty() ?>
        </td>


        <td class="edit">
            <p> Edit: </p>
        <a href="/products/edit/<?= $product->getId() ?>">Edit</a>
        </td>

        <td class="show">
            <p> Show: </p>
            <a href="/products/show/<?= $product->getId() ?>">Show Product</a>
        </td>
    <td>
        <form method="post" action="/products/delete">
            <p> Delete: </p>
            <input type="hidden" name="id" value="<?= $product->getId() ?>">
            <input type="submit" value="X">
        </form>
    </td>
        </tr>
    </table>

<?php endforeach; ?>
    </div>
<hr>