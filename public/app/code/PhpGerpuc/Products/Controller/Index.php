<?php

namespace PhpGerpuc\Products\Controller;

use PhpGerpuc\Framework\Core\Controller;
use PhpGerpuc\Framework\Helper\FormBuilder;
use PhpGerpuc\Framework\Helper\Request;
use \PhpGerpuc\Products\Model\Product;
use \PhpGerpuc\Framework\Helper\Url;
use PhpGerpuc\Framework\Helper\SqlBuilder;
use \PhpGerpuc\Products\Model\Collection\Products;
use \PhpGerpuc\Categories\Model\Collection\Categories;

class Index extends Controller
{
    public const PRODUCT_IMPORT_XML_URL = 'https://manopora.lt/ca/spc.xml';
    public const PRODUCT_IMPORT_STOCKS_CSV_URL = 'https://manopora.lt/ca/stocks.csv';
    private $post;
    private $request;

    public function __construct()
    {
        $request = new Request();
        $this->post = $request->getPost();
        parent::__construct('PhpGerpuc\Products');
    }


    public function index()
    {
        $productsCollection = new Products();
        $data['products'] = $productsCollection->getCollection();
        $this->render('admin/list', $data);
    }

    public function edit($id)
    {
        $product = new Product();
        $product = $product->load($id);
        $form = new FormBuilder('POST', '/products/store', 'form', 'product-form');
        $categories = new Categories(); //kad priskirti categorijoms

        $form->input('text', 'name', 'input-text', 'name', 'Product Name', '', '', $product->getName())
            ->input('text', 'sku', 'input-text', 'sku', 'SKU', '', '', $product->getSku())
            ->input('hidden', 'id', 'input-text', 'id', 'id', '', '', $product->getId())
            ->input('number', 'price', 'input-text', 'price', 'Price', '', '', $product->getPrice())
            ->textarea('description', 'Product description', '', $product->getDescription())
            ->input('number', 'cost', 'input-text', 'cost', 'cost', '', '', $product->getCost())
            ->input('number', 'qty', 'input-text', 'qty', 'Qty', '', '', $product->getQty());
        foreach ($categories->getCollection() as $category) {
            in_array($category->getId(), $product->getCategories()) ? $checked = true : $checked = false;

            $form->input(
                'checkbox',
                'categories[]',
                '',
                'category' . $category->getId(),
                '',
                $category->getName(),
                '',
                $category->getId(),
                $checked
            );
        }
        $form->button('save', 'save');

        $data['form'] = $form->get();
        $deleteForm = new FormBuilder('POST', '/products/delete');
        $deleteForm->button('delete', 'delete')
            ->input('hidden', 'id', 'input-text', 'id', 'id', '', '', $product->getId());
        $data['form2'] = $deleteForm->get();
        $data['title'] = 'Edit product';
        $this->render('form\create', $data);
    }

    public function show($id) {
        $product = new Product();
        $product = $product->load($id);
        $form = new FormBuilder('POST', '/products/store', 'form', 'product-form');



        $data['form'] = $form->get();

        $data['title'] =   $product->getName();
        $data['name'] =  $product->getName();
        $data['description'] =  $product->getDescription();
        $data['price'] = $product->getPrice();
        $data['qty'] =   $product->getQty();
        $this->render('admin\show', $data);
    }


    public function create()
    {
        $categories = new Categories();

        $formHelper = new FormBuilder('POST', Url::getUrl('products/store'), 'form-group form', 'product-form');
        $formHelper->input('text', 'name', 'form-control', 'product-name', 'Product Name')
            ->input('text', 'sku', 'form-control', 'product-sku', 'Product Sku')
            ->input('text', 'price', 'form-control', 'product-price', 'Product Price')
            ->textarea('description', 'Product description')
            ->input('number', 'cost', 'form-control', 'product-cost', 'cost')
            ->input('number', 'qty', 'form-control', 'product-qty', 'qty')
            ->button('ok', 'create product', 'btn btn-info');

        foreach ($categories->getCollection() as $category) {
            $formHelper->input(
                'checkbox',
                'categories[]',
                'form-check-input mx-2',
                'category' . $category->getId(),
                '',
                $category->getName(),
                '',
                $category->getId()
            );
        }
        $data['title'] = 'Create new product';
        $data['form'] = $formHelper->get();
        $this->render('form/create', $data);
    }



    public function store()
    {
        $product = new Product();
        if (isset($this->post['id'])) {
            $product = $product->load($this->post['id']);
        }
        if ($product->getSku() === $this->post['sku'] || Product::isSkuUniq((string)$this->post['sku'])) {
            $product->setName($this->post['name']);
            $product->setDescription($this->post['description']);
            $product->setSku($this->post['sku']);
            $product->setPrice($this->post['price']);
            $product->setQty($this->post['qty']);
            $product->setCost($this->post['cost']);
            $product->setCategories($this->post['categories']);
            $product->save();
            header('Location:' . Url::getUrl('products'));
        } else {
            echo 'SKU ' . $this->post['sku'] . ' jau yra užimtas';
        }
    }

    public function delete()
    {
        $this->id = $this->post['id'];
        $product = new Product();
        $product->load($this->id)->delete();
        header('Location: http://127.0.0.1:8001/products');
    }

    public function import()
    {

        // path where we will store our XML
        $path = PROJECT_ROOT_DIR . 'var\import\\';
        $fileName = 'products' . date('d-m-Y') . '.xml';

        //path + file name
        $file = $path . $fileName;

        //opened file handler from above
        $fp = fopen($file, 'w+');

        //init curl handler with targer xml url
        $curlHandler = curl_init(self::PRODUCT_IMPORT_XML_URL);
        // print_r($curlHandler);
        curl_setopt($curlHandler, CURLOPT_FILE, $fp);
        curl_exec($curlHandler);

        $xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);
        foreach ($xml->produkt as $product) {
            $newProduct = new Product();
            $sku = (string)$product->kzs;
            if (!Product::isSkuUniq($sku)) {
                continue;
            }
            $name = (string)$product->nazwa;
            $qty = (int)$product->status;
            $price = self::convertPrice((float)$product->cena_zewnetrzna);
            $description = (string)$product->dlugi_opis;
            $cost = self::convertPrice((float)$product->cena_zewnetrzna_hurt);

            $newProduct->setSku($sku);
            $newProduct->setName($name);
            $newProduct->setQty($qty);
            $newProduct->setPrice($price);
            $newProduct->setDescription($description);
            $newProduct->setCost($cost);

            $newProduct->save();
        }
    }


    public function stocks()
    {
        $pathToSave = PROJECT_ROOT_DIR . 'var\import\stocks\\';
        $fileName = 'products_stocks' . date('d-m-Y') . '.csv';
        $file = $pathToSave . $fileName;
        $fp = fopen($file, 'w+');
        $curlHandler = curl_init(self::PRODUCT_IMPORT_STOCKS_CSV_URL);
        curl_setopt($curlHandler, CURLOPT_FILE, $fp);
        curl_exec($curlHandler);

        $stocks = array_map('str_getcsv', file($file));
        array_walk($stocks, function (&$a) use ($stocks) {
            $a = array_combine($stocks[0], $a);
        });
        array_shift($stocks);

        foreach ($stocks as $stockItem) {
            if (!Product::isSkuUniq($stockItem['sku'])) {
                $product = new Product();
                $product->loadBySku($stockItem['sku']);
                $product->setQty($stockItem['qty']);
                $product->setSkipCategories(true);
                $product->save();
            }
        }
        header('Location:' . Url::getUrl('products'));
    }

    public function export()
    {
        $productCollection = new Products();
        $productCollection = $productCollection->getCollection();
        $productsToCsv = [];
        $productsToCsv[] = ['id', 'name', 'sku', 'qty', 'price'];
        foreach ($productCollection as $product) {
            $productsToCsv[] = [
                $product->getId(),
                $product->getName(),
                $product->getSku(),
                $product->getQty(),
                $product->getPrice()
            ];

        }
        $pathToSave = PROJECT_ROOT_DIR . 'var\export\\';
        $fileName = 'products' . date('d-m-Y') . '.csv';
        $file = $pathToSave . $fileName;

        $fp = fopen($file, 'w');

        foreach ($productsToCsv as $line) {
            fputcsv($fp, $line);
        }

        fclose($fp);
    }

    private static function convertPrice($price)
    {
        return $price * 0.22;
    }
}

