<?php

namespace PhpGerpuc\Products\Model\Collection;

use PhpGerpuc\Framework\Helper\SqlBuilder;
use PhpGerpuc\Products\Model\Product;
use PhpGerpuc\Framework\Helper\Debug;

class Products
{
    private $collection = [];

    public function getCollection()
    {
        $db = new SqlBuilder;
        $productsIds =  $db->select('id')->from('products')->get();

        foreach ($productsIds as $element) {
            $product = new Product();
            $this->collection[] =  $product->load($element['id']);
        }
        return $this->collection;
    }

    public function addCategoryFilter($id)
    {
        //select product_id from products_categories where category_id = $id;
        $db = new SqlBuilder;
        $productsIds = $db->select('product_id')->from('category_products')->where('category_id', $id)->get(); // get apsirasyt
        $productsIds = $this->cleanResults($productsIds);
        foreach ($this->collection as $product) {
            if (!in_array($product->getId(), $productsIds)) {
                unset($this->collection[$product->getId()]);
            }
        }
    }

    private function cleanResults($result) {
        $cleanArr = [];
        foreach ($result as $element) {
            foreach( $element as $line) {
                $cleanArr[] =  $line;
            }
        }
        return $cleanArr;
    }
}
