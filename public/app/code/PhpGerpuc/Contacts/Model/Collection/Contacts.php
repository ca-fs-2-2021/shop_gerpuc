<?php

namespace PhpGerpuc\Contacts\Model\Collection;

use PhpGerpuc\Framework\Helper\SqlBuilder;
use PhpGerpuc\Contacts\Model\Contact;
use PhpGerpuc\Framework\Helper\Debug;

class Contacts
{
    private $collection = [];

    public function getCollection()
    {
        $db = new SqlBuilder;
        $contactsIds =  $db->select('id')->from('contacts')->get();

        foreach ($contactsIds as $element) {
            $contact = new Contact();
            $this->collection[] =  $contact->load($element['id']);
        }
        return $this->collection;
    }

}
