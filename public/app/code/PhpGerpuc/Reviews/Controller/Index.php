<?php

namespace PhpGerpuc\Reviews\Controller;

use PhpGerpuc\Framework\Core\Controller;

class Index extends Controller
{
    private $post;

    public function __construct()
    {
        $request = new Request();
        $this->post = $request->getPost();
        parent::__construct('PhpGerpuc\Products');
    }

    public function index()
    {
        echo 'default Products method';
    }

    public function edit($id)
    {
        $id = (int) $id;
        echo $id;
    }
}