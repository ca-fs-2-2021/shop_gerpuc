<?php

namespace PhpGerpuc\Framework\Helper;


class Url
{
    public static function getUrl($path)
    {
        return APP_DOMAIN . $path;
    }

    public static function getCssUrl($file) {
        return APP_DOMAIN . 'css/' . $file . '.css';
    }

    public static function getJsUrl($file) {
        return APP_DOMAIN . 'js/' . $file . '.js';

    }

    public static function getImgUrl($file) {
        return APP_DOMAIN . 'images/' . $file;

    }
}
