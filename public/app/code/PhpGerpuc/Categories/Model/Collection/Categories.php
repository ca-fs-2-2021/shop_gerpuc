<?php

namespace PhpGerpuc\Categories\Model\Collection;

use PhpGerpuc\Framework\Helper\SqlBuilder;
use PhpGerpuc\Categories\Model\Category;

class Categories
{
    private $collection = [];

    public function getCollection()
    {
        $db = new SqlBuilder();
        $categoriesIds = $db->select('id')->from('categories')->get();

        foreach ($categoriesIds as $row) {
            $category = new Category();
            $this->collection[] = $category->load($row['id']);
        }
        return $this->collection;
    }
}