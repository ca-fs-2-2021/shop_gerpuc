<?php

use PhpGerpuc\Framework\Helper\Request;
use PhpGerpuc\Framework\Helper\Routing;

require_once '../app/config/env.php';
require_once '../vendor/autoload.php';

$request = new Request;
$routes = new Routing;

$url = $request->getRoute();

$routes->getControllerClass($url);



