<div class="wrapper">
    <h2 class="text-center my-4 text-uppercase"> <?php echo $data['title'] ?></h2>
    <?php echo $data['form']; ?>
    <hr>
</div>