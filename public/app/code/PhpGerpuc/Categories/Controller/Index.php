<?php

namespace PhpGerpuc\Categories\Controller;

use PhpGerpuc\Framework\Core\Controller;
use PhpGerpuc\Framework\Helper\FormBuilder;
use \PhpGerpuc\Categories\Model\Category;
use PhpGerpuc\Framework\Helper\Request;
use PhpGerpuc\Categories\Model\Collection\Categories;

class Index extends Controller
{
    private $post;

    public function __construct()
    {
        $request = new Request();
        $this->post = $request->getPost();
        parent::__construct('PhpGerpuc\Categories');
    }

    public function index()
    {
        $data['title'] = 'Category list';
        $categoriesCollection = new Categories();
        $data['categories'] = $categoriesCollection->getCollection();
        $this->render('admin/list', $data);
    }

    public function show()
    {
//        $productsModel = new Products();
//        $productsModel->addCategoryFilter($id);
//        $products = $productsModel->getCollection();
//        echo '<pre>' ;
//        print_r($products);
    }

    public function create()
    {

        $categories = new Categories();
        $categoriesOptions = [];
        $categoriesOptions[0] = '---------';
        foreach ($categories->getCollection() as $category) {
            $categoriesOptions[$category->getId()] = $category->getName();
        }

        $formHelper = new FormBuilder('POST', '/categories/store');
        $formHelper->input('text', 'name', '', '', 'category Name')
            ->input('text', 'slug', '', '', 'Slug')
            ->select('parent_id', $categoriesOptions, '')
            ->button('ok', 'create category', 'btn btn-info');

        $data['title'] = 'Create category';
        $data['form'] = $formHelper->get();
        $this->render('form\create', $data);
    }

    public function store()
    {
        $category = new Category();
        $category->setName($this->post['name']);
        $category->setSlug($this->post['slug']);
        $category->setParent_id($this->post['parent_id']);
        $category->save();
    }

    public function edit($id)
    {
        $categories = new Categories();
        $category = new Category();
        $category->load($id);
        $categoriesOptions = [];
        $categoriesOptions[0] = '--------';
        foreach ($categories->getCollection() as $category) {
            $categoriesOptions[$category->getId()] = $category->getName();
        }

        $formHelper = new FormBuilder('POST', '/categories/store', 'form-group form');
        $formHelper->input('text', 'name', '', 'category-name', '', '', $category->getName())
            ->input('text', 'slug', '', '', 'Slug', '', '', $category->getSlug())
            ->select('parent_id', $categoriesOptions, '')
            ->button('ok',  'btn btn-info', 'Edit');

        $data['title'] = 'Edit category';
        $data['form'] = $formHelper->get();
        $this->render('form\create', $data);
    }

    public function delete()
    {
        $this->id = $this->post['id'];
        $category = new Category();
        $category->load($this->id)->delete();
        header('Location:' . Url::getUrl('categories'));
    }

    public function update()
    {
        $id = $this->request->getPost('id');
        $name  = $this->request->getPost('name');
        $slug = $this->request->getPost('slug');
        $parent_id  = $this->request->getPost('parent_id');

        $category = new Category();
        $category->load($id);
        $category->setName($name);
        $category->setSlug($slug);
        $category->setparent_id($parent_id);
        $category->save($id);
    }
}
