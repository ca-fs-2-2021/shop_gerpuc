<?php

namespace PhpGerpuc\Contactscategories\Controller;

use PhpGerpuc\Framework\Core\Controller;
use PhpGerpuc\Framework\Helper\FormBuilder;
use \PhpGerpuc\Contactscategories\Model\Contactcategory;
use PhpGerpuc\Framework\Helper\Request;
use \PhpGerpuc\Contactscategories\Model\Collection\Contactscategories;
use PhpGerpuc\Framework\Helper\Url;

class Index extends Controller
{
    private $post;

    public function __construct()
    {
        $request = new Request();
        $this->post = $request->getPost();
        parent::__construct('PhpGerpuc\Contactscategories');
    }

    public function index()
    {
        $data['title'] = 'Category list';
        $contactscategoriesCollection = new ContactsCategories();
        $data['contactscategories'] = $contactscategoriesCollection->getCollection();
        $this->render('admin/list', $data);
    }

    public function create()
    {
        $contactscategories = new ContactsCategories();
        $categoriesOptions = [];
        $categoriesOptions[0] = '---------';
        foreach ($contactscategories->getCollection() as $contactcategory) {
            $categoriesOptions[$contactcategory->getId()] = $contactcategory->getName();
        }

        $formHelper = new FormBuilder('POST', '/contactscategories/store');
        $formHelper->input('text', 'name', '', '', 'category Name')
            ->button('ok', 'create category', 'btn btn-info');

        $data['title'] = 'Create category';
        $data['form'] = $formHelper->get();
        $this->render('form\create', $data);
    }

    public function store()
    {
        $contactcategory = new Contactcategory();
        $contactcategory->setName($this->post['name']);
        $contactcategory->save();
        header('Location:' . Url::getUrl('contacts/create'));

    }

    public function delete()
    {
        $this->id = $this->post['id'];
        $contactcategory = new Contactscategories();
        $contactcategory->load($this->id)->delete();
        header('Location:' . Url::getUrl('contactscategories'));
    }

    public function update()
    {
        $id = $this->request->getPost('id');

        $category = new Category();
        $category->load($id);
        $category->setName($name);

        $category->save($id);
    }
}
