
<div class="wrapper container">
    <h3 class="my-4"> <?php echo $data['title'] ?></h3>
    <?php echo $data['form']; ?>
    <hr>

    <p> <span class="h5"> Product name: </span> <?php echo $data['name'] ?> </p>
    <p> <span class="h5"> Product description: </span> <?php echo $data['description'] ?> </p>
    <p> <span class="h5"> Product price: </span> <?php echo $data['price'] ?> </p>
    <p> <span class="h5"> Product qty: </span> <?php echo $data['qty'] ?> </p>
</div>