<?php

namespace PhpGerpuc\Home\Controller;

use \PhpGerpuc\Framework\Core\Controller;
use \PhpGerpuc\Framework\Helper\Request;


class Index extends Controller
{

    private $request;

    public function __construct()
    {
        $this->request = new Request();
        parent::__construct('PhpGerpuc\Home');
    }

    public function index()
    {
        echo 'Our Home page';
    }
}
